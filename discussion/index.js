// Create a standard server app using Node.JS.

// get the http module using the require() directive. Then, repackage the module on a new variable.
	const http = require("http");

	const host = 4000;
// Create the server and place it inside a new variable to give it an identifier.

	// Arguments => They are used to "Catch" data and pass it along inside our function.
	let server = http.createServer((req, res) => {
		res.end("Welcome to the App");
	})

// Assign a designated port that will serve the project, by binding the connection with the desired port.
	server.listen(host);

// Include a response that will be displayed in the console to verify that the connection was established.
	console.log(`Listening on port: ${host}`);